from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    items = TodoList.objects.all
    context = {"items": items}
    return render(request, "todo_lists/list.html", context)


def todo_list_detail(request, id):
    details = TodoList.objects.get(id=id)
    context = {"details": details}
    return render(request, "todo_lists/details.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.id)
    else:
        form = TodoListForm
    context = {"form": form}
    return render(request, "todo_lists/create.html", context)


def edit_todolist(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect(todo_list_detail, id)
    else:
        form = TodoListForm(instance=todolist)
    context = {"form": form}
    return render(request, "todo_lists/edit.html", context)


def delete_todolist(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todo_lists/delete.html")


def create_todolist_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save(False)
            todoitem.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm
    context = {"form": form}
    return render(request, "todo_lists/createTask.html", context)


def todo_item_update(request, id):
    todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    context = {"form": form}
    return render(request, "todo_lists/editTask.html", context)
