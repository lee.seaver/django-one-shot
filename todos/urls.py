from django.urls import path
from todos.views import (
    todo_list_list,
    todo_list_detail,
    create_todolist,
    edit_todolist,
    delete_todolist,
    create_todolist_item,
    todo_item_update,
)

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>", todo_list_detail, name="todo_list_detail"),
    path("create/", create_todolist, name="create_todolist"),
    path("<int:id>/edit/", edit_todolist, name="edit_todolist"),
    path("<int:id>/delete/", delete_todolist, name="delete_todolist"),
    path("items/create/", create_todolist_item, name="create_todolist_item"),
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
]
